<?php
include "SquarePolygon.php";
include "RectanglePolygon.php";
include "TrianglePolygon.php";

/**
 * Get principal function
 */

function area(Polygon $obj)
{
  $obj->calc();
}

$square = new SquarePolygon;
$rectangle = new RectanlgePolygon;
$triangle = new TrianglePolygon;

/**
 * call to polymorfism
 */
area($square);
area($rectangle);
area($triangle);
