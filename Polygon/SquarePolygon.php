<?php
include './Polygon.php';

/**
 * Next level in Class hierarchy
 */

class SquarePolygon extends Polygon
{
  public function calc()
  {
    echo "\nArea of ​​a square: a = l x l\n";
  }
}
