<?php
include "./AnimalBiped.php";
include "./AnimalQuadruped.php";
/**
 * Instantiate
 */

/**
 * Function to invoke polymorfism
 */

function kindWalk(Animal $animal)
{
  $animal->walk();
}

$animalBiped = new AnimalBiped(125);
$animalQuadruped = new AnimalQuadruped(12);

kindWalk($animalBiped);
kindWalk($animalQuadruped);
