<?php

/**
 * Class
 */

class Animal
{
  protected $age;

  public function __construct($age)
  {
    //setter
    $this->age = $age;
  }

  public function older()
  {
    //getter
    return $this->age += 1;
  }
}

$cat = new Animal(2);

$dog = new Animal(3);

echo "\nThe new age Cat, is: " . $cat->older() . "\n";

echo "\nThe new age Dog, is: " . $dog->older() . "\n";
